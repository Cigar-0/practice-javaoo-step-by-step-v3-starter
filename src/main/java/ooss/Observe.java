package ooss;

public interface Observe {
    void update( Student leader);
}
