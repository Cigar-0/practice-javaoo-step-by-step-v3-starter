package ooss;

import java.util.ArrayList;
import java.util.List;

public class Teacher extends Person implements Observe{
    private final List<Integer> klass=new ArrayList<>();
    public Teacher(int id, String name, int age) {
        super(id, name, age);
    }

    @Override
    public String introduce() {
        String introduction;
        introduction = super.introduce() + " I am a teacher.";
        StringBuilder klassSet = new StringBuilder();
        if(klass.size() != 0){
            for(int i =0;i<klass.size();i++){
                if(klassSet.length() > 0){
                    klassSet.append(", ");
                }
               klassSet.append(klass.get(i));
            }
            introduction = introduction+" I teach Class "+klassSet+".";
        }

        return introduction;
    }

    public void assignTo(Klass klass) {
        this.klass.add(klass.getNumber());
    }
    public boolean belongsTo(Klass klass){
        return this.klass.contains(klass.getNumber());
    }
    public boolean isTeaching(Student stu){
        return this.klass.contains(stu.getKlass().getNumber());
    }
    @Override
    public void update(Student leader) {
        System.out.println("I am " + this.getName() + ", teacher of Class " + leader.getKlass().getNumber() + ". I know " + leader.getName() + " become Leader.");
    }

    private String getName() {
        return this.name;
    }
}
