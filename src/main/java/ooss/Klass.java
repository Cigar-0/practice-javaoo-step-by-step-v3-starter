package ooss;

import java.util.Objects;

public class Klass {
    private int number;
    private Student leader;
    private Observe observe;

    public Klass(int number) {
        this.number = number;
    }


    public int getNumber() {
        return number;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Klass klass = (Klass) o;
        return number == klass.number;
    }

    public void assignLeader(Student stu) {
        if(!stu.isIn(this)){
            System.out.println("It is not one of us.");
        }
        this.leader = stu;
        if (observe!=null){
            notifyObserver();
        }
    }
    public boolean isLeader(Student stu){
        return this.leader == stu;
    }

    public Student getLeader() {
        return this.leader;
    }
    private void notifyObserver() {
        this.observe.update(leader);
    }

    public void attach(Observe observe) {
        this.observe=observe;
    }
}
