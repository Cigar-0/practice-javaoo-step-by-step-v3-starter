package ooss;

import java.util.Objects;

public class Student extends Person implements Observe{
    private Klass klass;

    public Student(int id, String name, int age, Klass klass) {
        super(id, name, age);
        this.klass = klass;
    }


    public Student(int id, String name, int age) {
        super(id, name, age);
    }

    @Override
    public String introduce() {
        String introduction;
        introduction = Objects.isNull(this.klass) ? super.introduce()+" I am a student." : super.introduce()+" I am a student. I am in class "+ this.klass.getNumber() +".";
        if(this.klass.getLeader() == this){
            introduction = super.introduce()+" I am a student."+" I am the leader of class "+this.klass.getNumber()+".";
        }
        return introduction;
    }
    public void join(Klass klass){
        this.klass = klass;
    }
    public boolean isIn(Klass klass){
        return klass == this.klass;
    }

    public Klass getKlass() {
        return this.klass;
    }

    public String getName() {
        return this.name;
    }
    public void update(Student leader) {
        System.out.println("I am " + this.getName() + ", student of Class " + leader.getKlass().getNumber() + ". I know " + leader.getName() + " become Leader.");
    }

}
